Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: noweb
Source: https://github.com/nrnrnr/noweb

Files: *
Copyright: 1989-2018 Norman Ramsey
License: noweb or BSD-2-clause

Files: debian/*
Copyright:
 1997-2003 Federico Di Gregorio <fog@perosa.alpcom.it>
 2006-2016 Hubert Chan <uhoreg@debian.org>.
License: noweb

License: noweb
 Noweb is protected by copyright.  It is not public-domain
 software or shareware, and it is not protected by a ``copyleft''
 agreement like the one used by the Free Software Foundation.
 .
 Noweb is available free for any use in any field of endeavor.  You may
 redistribute noweb in whole or in part provided you acknowledge its
 source and include this COPYRIGHT file.  You may modify noweb and
 create derived works, provided you retain this copyright notice, but
 the result may not be called noweb without my written consent.
 .
 You may sell noweb if you wish.  For example, you may sell a CD-ROM
 including noweb.
 .
 You may sell a derived work, provided that all source code for your
 derived work is available, at no additional charge, to anyone who buys
 your derived work in any form.  You must give permisson for said
 source code to be used and modified under the terms of this license.
 You must state clearly that your work uses or is based on noweb and
 that noweb is available free of change.  You must also request that
 bug reports on your work be reported to you.

License: BSD-2-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
